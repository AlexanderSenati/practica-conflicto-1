@extends('layout')    
@section('content')

<div class="container">
    
<table class="table" border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($alumnos as $alumno)
            <tr>
                <td>{{ $alumno->id }}</td>
                <td>{{ $alumno->nombres }}</td>
                <td>{{ $alumno->apellidos }}</td>
                <td>    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection